import unittest
from hw2 import *


class TestHomework2(unittest.TestCase):

    def setUp(self):
        pass

    # string_distance
    def test_string_distance(self):
        self.assertEqual(string_distance('cba', 'abc'), 2)
        self.assertEqual(string_distance('ABCD', 'abc'), 1)

    # chars after
    def test_chars_after(self):
        self.assertEqual(chars_after('cba', 'abc'), ['c'])
        res = chars_after('Nine', 'diner')
        self.assertEqual(len(res), 2)
        self.assertEqual(set(res), {'d', 'r'})
        self.assertTrue(isinstance(res, list))

    # convert_date
    def test_convert_date(self):
        self.assertEqual(convert_date('5/1/1998'), 'May 1, 1998')
        self.assertEqual(convert_date("01/001/2000"), 'January 1, 2000')

    # my_sort
    def test_my_sort(self):
        l1 = [5, 2, 1, 4, 3]
        self.assertEqual(my_sort(l1), sorted(l1))
        l2 = ['c', 'a', 'b']
        self.assertEqual(my_sort(l2), sorted(l2))

    # frequency dict
    def test_frequency_dict(self):
        ans1 = 'e=[1], h=[0], l=[2, 3], o=[4]'
        self.assertEqual(frequency_dict('hello'), ans1)
        ans2 = ' =[2], 0=[3, 4], a=[0, 1]'
        self.assertEqual(frequency_dict('aa 00'), ans2)

    # my_permute
    def test_my_permute(self):
        ans1 = [['a', 'b'], ['b', 'a']]
        self.assertEqual(my_permute(['a', 'b']), ans1)
        ans2 = [[1, 2, 3], [1, 3, 2], [2, 1, 3],
                [2, 3, 1], [3, 1, 2], [3, 2, 1]]
        self.assertEqual(my_permute([1, 2, 3]), ans2)
        ans3 = [[1, 1]]
        self.assertEqual(my_permute([1, 1]), ans3)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
