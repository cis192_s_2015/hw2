# Homework 2
# Due Sunday, Feb 1 at 23:59
# Always write the final code yourself
# Cite any websites you referenced
# Use the PEP-8 checker for full style points:
# https://pypi.python.org/pypi/pep8


def string_distance(s1, s2):
    '''Counts the number of positions at which two strings have different
    characters. If the strings are of unequal length, you should count the
    missing characters as disagreements. Do not count uppercase and
    lowercase letters as different.
    '''
    pass


def chars_after(s1, s2):
    '''Return a list of unique characters in s2 such that the index at
    which the character first occurs in s2 is greater than the index
    that the character first occurs in s1. If a character does not
    occur in s1 but occurs in s2 then it should be among those
    returned. The order of chars in the returned list does not matter.
    Different case (Upper/lower) counts as different characters this time
    '''
    pass


def convert_date(s):
    ''' Converts a date string of the form "mm/dd/yyyy" to a string of the
    form "Month date, year". You should be able to handle (any number of)
    leading zeros, but can assume the input will correspond to a valid date.
    The month_name attribute of the calendar module may be useful:
    https://docs.python.org/3.4/library/calendar.html
    '''
    pass


def my_sort(lst):
    '''Return a sorted copy of a list. Do not modify the original list. Do
    not use Python's built in sorted method or [].sort(). You may use
    any sorting algorithm of your choice.  Running time does not
    matter.
    '''
    pass


def frequency_dict(s):
    ''' First create a dictionary where each key is a character that appears
    in the input string, and the value is a list of the indices at which that
    character appears. Then format this dictionary as seen in the examples,
    and return the resulting string.

    Treat lower and upper case characters the same, and just include the
    lowercase version in your dictionary. Pairs should be listed in
    alphabetical order by key (use your my_sort function for this).
    '''
    pass


def my_permute(lst):
    ''' Returns a list of all permutations of a list. Do not use any of
    Python's builtin functions from the itertools library. Sort the list
    using my_sort before returning it, and remove any duplicates as well.
    Note that the set() trick won't work right off the bat -- why not?
    '''
    pass


def main():
    pass

if __name__ == "__main__":
    main()
